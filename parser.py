
###############################################################################################################
#Auther: Yifan Wang
#Objective: This script is dedicated to parse csv data into python objects, and push data onto the server.
#Date: 03/10/2018
#
#
###############################################################################################################

import csv
import requests
import sys
from datetime import datetime
import logging

#Global parameters
url = 'http://localhost:7272/api/v0.1/0e49a8de9e74404da73fadde4f6c393e/push'

#--------------------------------------------------------------------------------------------------------------
#Setting up the logger
logger = logging.getLogger('endlesscode')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
file_handler = logging.FileHandler("parserLog.log")  
file_handler.setFormatter(formatter) 
logger.addHandler(file_handler)
#logger.setLevel(logging.INFO) default level is warning

#--------------------------------------------------------------------------------------------------------------
#Error classes used in parser.py

class Error(Exception):
	"""Base class for exceptions in this module."""
	pass

class InvalidDataFormatError(Error):
	"""Exception raised for invalid format in the source csv.

    Attributes:
        file_name -- input csv's file_name
        message -- explanation of the error
    """
	def _init_(self, message):
		self.message = message
		super(Error,self)._init_(message)

#--------------------------------------------------------------------------------------------------------------
#Functions that master can call

#This function takes a python dictionary as data
#And pushes the data onto tipboard server
def push(data):
	response = requests.post(url, data = data)


#This function takes a csv file name, and opens the file
#It returns a reader object which will iterate over lines in the given csv file
def open_csv(file_name):
	try:
		f = open(file_name,"rb")

	except IOError as e:
		print ('Handling runtime error', e)
		raise
		
	else:
		reader = csv.reader(f)
		objects={"csv":reader, "file":f}
		return objects

#This function takes the file name of the text-type csv, and the id of the target tile
#Then parses the csv into python dict, and push it onto the target tile
def parse_text_csv(file_name, target):

	try:
		#open the csv file
		objects = open_csv(file_name)
		reader = objects["csv"]
		f = objects["file"]
		#get data off the reader object
		data_list = []
		for row in reader:
			data_list.append(row)

	except IOError as IOe:
		#append error message in the log file. First exception will generate the log file.
		logger.error('Failed to open the file: ' + file_name + str(IOe))

	else:
		value = ""
		
		for row in data_list:
			value += ",".join(row)
			value += "."

		f.close()		
			
		value = '{"text":"'+ value + '"}'
		data = [("tile","text"),("key",target),("data",value)]
		
		#push data onto the server
		print data
		push(data)

#This function takes the file name of the pie_chart-type csv, the id of the target, and the pie_chart's title
#Then parses the csv into python dict, and push it onto the target tile
def parse_pie_csv(file_name, target):

	try:
		#open the file
		objects = open_csv(file_name)
		reader = objects["csv"]
		f = objects["file"]
		#get data off reader object
		data_list = []
		for row in reader:
			data_list.append(row)
		#check for data format
		#First row should only have one element
		if (len(data_list[0]) > 1):
			raise InvalidDataFormatError(file_name + ' first row is not title-format')
		else:
			#The rest of the rows should all have 2 elements
			for i, item in enumerate(data_list):
				if(i != 0):
					if(len(item) > 2):
						raise InvalidDataFormatError(file_name + ' row number ' + str(i + 1) + ' has too many arguements')
					if(len(item) <= 1):
						raise InvalidDataFormatError(file_name + ' row number ' + str(i + 1) + ' has not enough arguements')
						

	except InvalidDataFormatError as e:
		print ('Handling runtime error', e)
		logger.error('Failed to parse the file: ' + str(e))
	except IOError as IOe:
		logger.error('Failed to open the file: ' + file_name + str(IOe))

	else:
		#set global variables
		value = ''
		title = ''
		for i, item in enumerate(data_list):
			if(i != 0):
				temp_str = item[0]
				temp_int = item[1]
				temp_row = '["' + temp_str + '", ' + temp_int + ']'
				value = value + temp_row + ","
			else:
				title = item[0]
		#strip the ending comma
		value = value[:-1]

		f.close()

		value = '{"title":"' + title + '",' + ' "pie_data":[' + value + "]}"
		data = [
		 ('tile', 'pie_chart'), 
		 ('key', target),
		 ('data', value)
		]
		print(data)

		#Push data onto the server
		push(data)


#This function takes the file name of the cumulative_flow-type csv, the id of the target, and the cumulative_flow's title
#Then parses the csv into python dict, and push it onto the target tile
def parse_cumulative_csv(file_name, target):
	
	try:
		#open the file
		objects = open_csv(file_name)
		reader = objects["csv"]
		f = objects["file"]
		#get data off the reader object
		data_list = []
		for row in reader:
			data_list.append(row)

		#check data format before processing it
		#First row should only have one element
		if (len(data_list[0]) > 1):
			raise InvalidDataFormatError(file_name + ' first row is not title-format')
		else:
			#The rest of rows should all have at least 2 elements
			for i, item in enumerate(data_list):
				if(i != 0):
					if(len(item) <= 1):
						raise InvalidDataFormatError(file_name + ' row number ' + str(i + 1) + ' has not enough arguements')


	except InvalidDataFormatError as e:
		print ('Handling runtime error', e)
		logger.error('Failed to parse the file: ' + str(e))
	except IOError as IOe:
		logger.error('Failed to open the file: ' + file_name + str(IOe))

	else:
		#Processing data
		#concatenate data into a single python string
		title = data_list[0][0]
		value = ''
		for j, element in enumerate(data_list):
			if(j != 0):
				value = value + '{"label": "' + element[0] + '", "series": ['
				for i,item in enumerate(element):
					if item.isdigit():
						#if not the last number
						if (i != len(element)-1):
							value = value + item + ','
						#if it is the last number
						else:
							value = value + item + ']},'
					#ignore the label
					else:
						continue
		#strip the ending comma
		value = value[:-1]

		f.close()

		#Add on parameters
		value = '{"title": "' + title + '", "series_list": [' + value + ']}'
		
		data = [
		 ('tile', 'cumulative_flow'),
		 ('key', target),
		 ('data', value)
		]

		print data
		
		#Push data onto the server
		push(data)


#This function takes the file name of the big_value-type csv, the id of the target
#Then parses the csv into python dict, and push it onto the target tile

#"title" is in the 1st row of the csv file, instead of as a param on the function
def parse_big_value_csv(file_name, target):
	
	try:
		#open the file
		objects = open_csv(file_name)
		reader = objects["csv"]
		f = objects["file"]
		#get data off reader object
		data_list = []
		for row in reader:
			data_list.append(row)

		row0 = data_list[0]
		row1 = data_list[1]
		row2 = data_list[2]
		
		#check if the data is correct
		#First 3 rows should only have one element each
		if (len(data_list[0]) > 1):
			raise InvalidDataFormatError(file_name + ' first row is not title-format')
		elif (len(data_list[1]) > 1):
			raise InvalidDataFormatError(file_name + ' second row is not description-format')
		elif (len(data_list[2]) > 1):
			raise InvalidDataFormatError(file_name + ' third row is not big-value-format')
		#The whole data set should have 7 rows	max		
		elif (len(data_list) > 7):
			raise InvalidDataFormatError(file_name + ' has too many rows')
		else:
			#The rest of the rows should each have exactly 2 arguments
			for i, item in enumerate(data_list):
				if(i >= 3):
					if(len(item) > 2):
						raise InvalidDataFormatError(file_name + ' row number ' + str(i + 1) + ' has too many arguements')
					if(len(item) <= 1):
						raise InvalidDataFormatError(file_name + ' row number ' + str(i + 1) + ' has not enough arguements')

	except InvalidDataFormatError as e:
		print ('Handling runtime error', e)
		logger.error('Failed to parse the file: ' + str(e))
	except IOError as IOe:
		logger.error('Failed to open the file: ' + file_name + str(IOe))

	else:
		#If data format is correct, process data
		value = '{"title": "' + row0[0] + '", "description": "' + row1[0] + '", "big-value": "' + row2[0] + '", "'
		to_add = []
		for i,item in enumerate(data_list):
				#Skip the first 2 rows
				if(i >= 3):
					pair1 = ()
					pair2 = ()
					if(i == 3):
						pair1 = ('upper-left-label', item[0])
						pair2 = ('upper-left-value', item[1])
						to_add.append(pair1)
						to_add.append(pair2)
					elif(i == 4):
						pair1 = ('lower-left-label', item[0])
						pair2 = ('lower-left-value', item[1])
						to_add.append(pair1)
						to_add.append(pair2)
					elif(i == 5):
						pair1 = ('upper-right-label', item[0])
						pair2 = ('upper-right-value', item[1])
						to_add.append(pair1)
						to_add.append(pair2)
					elif(i == 6):
						pair1 = ('lower-right-label', item[0])
						pair2 = ('lower-right-value', item[1])
						to_add.append(pair1)
						to_add.append(pair2)

		for i, item in enumerate(to_add):
			if (i != len(to_add)-1):
				value = value + item[0] + '": "' + item[1] + '", "'

			else:
				value = value + item[0] + '": "' + item[1] + '"}'

		f.close()

		data = [('tile', 'big_value'),
				('key', target),
				('data', value)
				]
		print data

		push(data)

	

#This function takes the file name of the line_chart-type csv, the id of the target
#Then parses the csv into python dict, and push it onto the target tile

#"subtitle" is in the 1st row of the csv file, "description" is in the 2nd row of the csv file
def parse_line_csv(file_name, target):
	
	try:
		#open the file
		objects = open_csv(file_name)
		reader = objects["csv"]
		f = objects["file"]
		#get data off the reader object
		data_list = []
		for row in reader:
			data_list.append(row)

		#check data format before processing it
		if (len(data_list[0]) > 1) and data_list[0][1] != '':
			raise InvalidDataFormatError(file_name + ' first row is not subtitle-format')
		elif (len(data_list[1]) > 1) and data_list[1][1] != '':
			raise InvalidDataFormatError(file_name + ' second row is not description-format')
		else:
			#The rest of rows should all have at least 2 elements
			for i, item in enumerate(data_list):
				if(i >= 3):
					if(len(item) <= 1):
						raise InvalidDataFormatError(file_name + ' row number ' + str(i + 1) + ' has not enough arguements')
		

	except InvalidDataFormatError as e:
		print ('Handling runtime error', e)
		logger.error('Failed to parse the file: ' + str(e))
	except IOError as IOe:
		logger.error('Failed to open the file: ' + file_name + str(IOe))

	else:
		#If data format is correct, process data
		value = ''
		subtitle = ''
		description = ''
		xaxis = []
		for i,item in enumerate(data_list):
			if(i == 0):
				subtitle = item[0]
			if(i == 1):
				description = item[0]
			if (i >= 2):
				#extract the third row
				if(i == 2):
					for element in item:
						xaxis.append(element)

					value = value + '['
				#if nott he third row also not the last row
				if(i > 2) and (i != len(data_list)-1):
					value = value + '['
					for j, element in enumerate(item):
						if (j != len(item)-1):
							value = value + '["' + xaxis[j] + '", ' + element + '], '
						else:
							value = value + '["' + xaxis[j] + '", ' + element + ']'

					value = value + '], '

				if (i > 2) and (i == len(data_list)-1):
					value = value + '['
					for j, element in enumerate(item):
						if (j != len(item)-1):
							value = value + '["' + xaxis[j] + '", ' + element + '], '
						else:
							value = value + '["' + xaxis[j] + '", ' + element + ']'

					value = value + ']]'
		value = '{"subtitle": "' + subtitle + '", "description": "' + description + '", "series_list": ' + value + '}'

		f.close()

		data = [('tile', 'line_chart'),
				('key', target),
				('data', value)
				]
		print data
		push(data)
		

	

