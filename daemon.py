
###############################################################################################################
#Auther: Yifan Wang
#Objective: This script is dedicated to monitor the directories, and invoke operations based on the changes.
#Date: 03/10/2018
#
#
###############################################################################################################

import requests
import pyinotify
from multiprocessing.pool import ThreadPool

import parser
import master

import json
import csv
import os
import sys

#This script takes a file path, and monitors file creations and deletions under this dir(and all sub-dirs)
monitored_path = "/home/yifan/codes"

#function definitions

#This function starts to monitor a directory using pynitify
def monitor():
	notifier.start()#starts a new thread



#--------------------------------------------------------------------------------------------------------------
#Main program

#configure a notifier
wm = pyinotify.WatchManager()  # Watch Manager
mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events

class EventHandler(pyinotify.ProcessEvent):
	def process_IN_CREATE(self, event):
		new_file_path = event.pathname
		print "Creating:", event.pathname
		master.processer(new_file_path)

	def process_IN_DELETE(self, event):
		print "Removing:", event.pathname

#log.setLevel(10)
notifier = pyinotify.ThreadedNotifier(wm, EventHandler())

#Watch all specified paths
wdd = wm.add_watch(monitored_path, mask, rec=True)


#Multithread here, start a thread to monitor a dir, and another thread to wait for user input to end the monitoring process
#First start monitoring
#Prompt to stop this monitoring thread
pool = ThreadPool(processes = 2 )
pool.apply_async(monitor)
async_result = pool.apply_async(master.get_input)
return_val = async_result.get()
#pool.join() ??

if return_val == "end":
	wm.rm_watch(wdd.values())
	notifier.stop()




