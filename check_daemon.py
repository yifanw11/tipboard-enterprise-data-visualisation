
###############################################################################################################
#Auther: Yifan Wang
#Objective: This script checks if the daemon process is sleeping or running.
#Date: 03/10/2018
#
#
###############################################################################################################

from datetime import datetime
from subprocess import check_output
import psutil



#This function runs a bash command "ps -p pid -o cmd" to check the cmd of a process by its pid
def get_cmd(pid):
	return check_output(["ps", "-p", str(pid), "-o", "cmd"])

#This function runs a bash command "ps -p pid -o state --no-headers" to check the status of a process by its pid
def check_status(pid):
	bash_output = check_output(["ps","-p",str(pid),"-o","state","--no-headers"])
	#srip the trialing newline
	status = bash_output[: len(bash_output) - 1]
	return status

ifAlive = 'not alive'
pids = []

#Get pids that are from python scripts
for proc in psutil.process_iter():
	try:
		#Get all pids
		pinfo = proc.as_dict(attrs = ["pid", "name", "username"])
	except psutil.NoSuchProcess:
		pass
	else:
		#Filter pids by process cmd name
		if pinfo["name"] == "python":
			pids.append(pinfo["pid"])


#Turn this into a function and add error handling
#Find daemon.py's pid in the python pid list
for item in pids:
	#String manipulation to get the python program's name
	full_cmd = get_cmd(item)
	last_cmd_start_pos = full_cmd.rfind('/')
	last_cmd = full_cmd[last_cmd_start_pos + 1: ]
	program_name_start_pos = last_cmd.rfind(' ')
	program_name = last_cmd[program_name_start_pos + 1: len(last_cmd) - 1]

	#Pick daemon.py's pid
	if (program_name == "daemon.py"):
		daemon_pid = item
		#print daemon_pid
		#print program_name

		#Check daemon.py's process status
		daemon_status = check_status(daemon_pid)
		if ((daemon_status == 'S') or (daemon_status == 'R')):
			ifAlive = 'alive'

#print daemon_status
#print ifAlive

myFile = open('daemon_log.txt','a')
myFile.write('Checked daemon.py on ' + str(datetime.now()) + '; daemon.py has a status of ' + daemon_status + ' and is ' + ifAlive + '\n')