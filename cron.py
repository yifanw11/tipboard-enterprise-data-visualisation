
###############################################################################################################
#Auther: Yifan Wang
#Objective: This script generates a cron job that invokes check_daemon.py every minute.
#Date: 03/10/2018
#
#
###############################################################################################################

from crontab import CronTab

cron = CronTab(user = True)
job = cron.new(command = 'python /home/yifan/codes/check_daemon.py')
job.minute.every(1)
job.enable()

for job in cron:
	print job

cron.write()