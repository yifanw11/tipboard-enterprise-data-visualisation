
###############################################################################################################
#Auther: Yifan Wang
#Objective: This script acts as a centualised control center for scheduling and manipulating all scripts.
#Date: 03/10/2018
#
#
###############################################################################################################

import requests
import pyinotify

import parser

import json
import csv
import os
import sys




#This function takes a path of a folder, and gives all the file names under that folder
def search_files(path):
	dirs = os.listdir(path)
	files = []
	for file in dirs:
		files.append(file)

	for file in files:
		print file

#This function returns the current working directory
def get_dir():
	cwd = os.getcwd()
	print cwd


#--------------------------------------------------------------------------------------------------------------
#Functions called in daemon.py

#This function takes an user input and stores that into a string
def get_input():
	print("key in 'end' to stop monitoring: ")
	user_input = raw_input()
	return user_input

#This function calls the parser and pushes the newly added data onto the dashboard
def decision_maker(target, folder_name, new_file_name):
	if folder_name == "text":
		parser.parse_text_csv(new_file_name, target)
	elif folder_name == "cumulative_flow":
		parser.parse_cumulative_csv(new_file_name, target)
	elif folder_name == "pie_chart":
		parser.parse_pie_csv(new_file_name, target)
	elif folder_name == "big_value":
		parser.parse_big_value_csv(new_file_name, target)
	elif folder_name == "line_chart":
		parser.parse_line_csv(new_file_name, target)


#This function gives the parent folder name of a file or a folder
def get_parent_folder_name(start_pos, new_file_path):
	start_pos = start_pos - len(new_file_path)
	truncated_path = new_file_path[:start_pos]
	dir_pos = truncated_path.rfind('/')
	folder_name = truncated_path[dir_pos + 1:]

	return folder_name

#This function process the monitoring outcomes, and gmakes responses
def processer(new_file_path):
	#find the index of the last '/'
	start_pos = new_file_path.rfind('/')
	#Get the newly added file's name
	new_file_name = new_file_path[start_pos + 1:]

	#Get the folder name where the newly added file belongs to, which is also the target id
	target = get_parent_folder_name(start_pos, new_file_path)

	#find the index of the second last '/' and get the datatype
	parent_path = new_file_path[0 : start_pos]
	start_pos2 = parent_path.rfind('/')
	folder_name = get_parent_folder_name(start_pos2, parent_path)
	
	#parse data based on the datatype and taget id
	decision_maker(target, folder_name, new_file_name)

#--------------------------------------------------------------------------------------------------------------
#Testers

#test search_files
#search_files("/home/yifan/codes/text")

#test get_dir
#get_dir()

#test the error handling and log.txt with the wrong csv file path
#parser.parse_text_csv("text.csv", "id_3")

#test parse_text_csv
#parser.parse_pie_csv("pie_chart/pie_chart.csv", "2_id_4", "Zscalar")

#test parse_cumulative_csv
#parser.parse_cumulative_csv("cumulative_flow/cumulative_flow.csv","id_1","Cool Title")
